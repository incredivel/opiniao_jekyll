---
layout: page
title: Sobre
---

O arquivamento de algumas humildes ideias neste meio avia o meu desejo de escrever consuetudinariamente em português.

Deixa-te estar e sente-te em minha casa.

<br>

[<img width="25" style="filter:grayscale(100%);" src="assets/twitter.png">](https://twitter.com/incredivel)
[<img width="23" style="filter:grayscale(100%);" src="assets/gitlab.ico">](https://gitlab.com/incredivel/opiniao)
[<img width="22" style="filter:grayscale(100%);" src="assets/goatcounter.png">](https://opiniao.goatcounter.com/)