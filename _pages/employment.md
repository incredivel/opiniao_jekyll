---
layout: page
title: Employment
---


1.  [Portugal nos 2010s](#orgd482f34)
2.  [Europa em 2019](#orgb9bac4a)

<a id="orgd482f34"></a>

## Portugal nos 2010s

*Carrega na imagem para acederes à versão interativa.*

<a href="/assets/vizs/employment/PT2010.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2010.svg"></a>
<a href="/assets/vizs/employment/PT2011.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2011.svg"></a>
<a href="/assets/vizs/employment/PT2012.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2012.svg"></a>
<a href="/assets/vizs/employment/PT2013.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2013.svg"></a>
<a href="/assets/vizs/employment/PT2014.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2014.svg"></a>
<a href="/assets/vizs/employment/PT2015.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2015.svg"></a>
<a href="/assets/vizs/employment/PT2016.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2016.svg"></a>
<a href="/assets/vizs/employment/PT2017.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2017.svg"></a>
<a href="/assets/vizs/employment/PT2018.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2018.svg"></a>
<a href="/assets/vizs/employment/PT2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2019.svg"></a>


<a id="orgb9bac4a"></a>

## Europa em 2019

*Carrega na imagem para acederes à versão interativa.*

<a href="/assets/vizs/employment/AT2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/AT2019.svg"></a>
<a href="/assets/vizs/employment/BE2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/BE2019.svg"></a>
<a href="/assets/vizs/employment/BG2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/BG2019.svg"></a>
<a href="/assets/vizs/employment/CY2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/CY2019.svg"></a>
<a href="/assets/vizs/employment/CZ2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/CZ2019.svg"></a>
<a href="/assets/vizs/employment/DE2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/DE2019.svg"></a>
<a href="/assets/vizs/employment/DK2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/DK2019.svg"></a>
<a href="/assets/vizs/employment/EE2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/EE2019.svg"></a>
<a href="/assets/vizs/employment/EL2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/EL2019.svg"></a>
<a href="/assets/vizs/employment/ES2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/ES2019.svg"></a>
<a href="/assets/vizs/employment/FI2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/FI2019.svg"></a>
<a href="/assets/vizs/employment/FR2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/FR2019.svg"></a>
<a href="/assets/vizs/employment/HR2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/HR2019.svg"></a>
<a href="/assets/vizs/employment/HU2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/HU2019.svg"></a>
<a href="/assets/vizs/employment/IE2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/IE2019.svg"></a>
<a href="/assets/vizs/employment/IT2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/IT2019.svg"></a>
<a href="/assets/vizs/employment/LT2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/LT2019.svg"></a>
<a href="/assets/vizs/employment/LU2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/LU2019.svg"></a>
<a href="/assets/vizs/employment/LV2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/LV2019.svg"></a>
<a href="/assets/vizs/employment/MT2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/MT2019.svg"></a>
<a href="/assets/vizs/employment/NL2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/NL2019.svg"></a>
<a href="/assets/vizs/employment/PL2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PL2019.svg"></a>
<a href="/assets/vizs/employment/PT2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/PT2019.svg"></a>
<a href="/assets/vizs/employment/RO2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/RO2019.svg"></a>
<a href="/assets/vizs/employment/SE2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/SE2019.svg"></a>
<a href="/assets/vizs/employment/SI2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/SI2019.svg"></a>
<a href="/assets/vizs/employment/SK2019.html" title="Versão interativa"><img width="100%" src="assets/vizs/employment/SK2019.svg"></a>

