---
layout: page
title: log
---

#### 2021-08

<details><summary>film | <b>Wings of Desire</b> (1987) Wim Wenders</summary><blockquote><a href="https://www.criterion.com/films/200-wings-of-desire">Criterion</a></blockquote></details>

<details><summary>film | <b>The Seventh Seal</b> (1957) Ingmar Bergman</summary><blockquote><a href="https://www.criterion.com/films/173-the-seventh-seal">Criterion</a></blockquote></details>

<details><summary>film | <b>Vivre sa vie</b> (1962) Jean-Luc Godard</summary><blockquote><a href="https://www.criterionchannel.com/vivre-sa-vie">Criterion</a></blockquote></details>

<details><summary>film | <b>8½</b> (1963) Federico Fellini</summary><blockquote><a href="https://www.criterionchannel.com/81-2">Criterion</a></blockquote></details>

<details><summary>film | <b>The Battle of Algiers</b> (1966) Gillo Pontecorvo</summary><blockquote><a href="https://www.criterion.com/films/248-the-battle-of-algiers">Criterion</a></blockquote></details>

<details><summary>film | <b>Bicycle Thieves</b> (1948) Vittorio De Sica</summary><blockquote><a href="https://www.criterion.com/films/210-bicycle-thieves">Criterion</a></blockquote></details>

<details><summary>film | <b>Stalker</b> (1979) Andrei Tarkovsky</summary><blockquote><a href="https://www.criterion.com/films/28150-stalker">Criterion</a></blockquote></details>

<details><summary>exhi | Museu Coleção Berardo</summary><blockquote><a href="https://pt.museuberardo.pt/">Website</a></blockquote></details>

#### 2021-06

<details><summary>film | <b>What Happened Was</b> (1994) Tom Noonan</summary><blockquote><a href="https://en.wikipedia.org/wiki/What_Happened_Was">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Ossos</b> (1997) Pedro Costa</summary><blockquote><a href="https://en.wikipedia.org/wiki/Ossos">Wikipedia</a></blockquote></details>

<details><summary>fic | Sena, Jorge de (1977) <b>O Físico Prodigioso</b></summary><blockquote>@book{sena1977prodigioso,
   title =     {O Físico Prodigioso},
   author =    {Sena, Jorge de},
   publisher = {SL},
   year =      {1977}}</blockquote></details>

<details><summary>course | <b>Introduction to Political Philosophy</b> (Yale) Steven B. Smith</summary><blockquote><a href="https://oyc.yale.edu/political-science/plsc-114">Yale</a></blockquote></details>

#### 2021-05

<details><summary>film | <b>Solaris</b> (1972) Andrei Tarkovsky</summary><blockquote><a href="https://en.wikipedia.org/wiki/Solaris_%281972_film%29">Wikipedia</a></blockquote></details>

<details><summary>course | <b>The American Novel Since 1945</b> (Yale) Amy Hungerford </summary><blockquote><a href="https://oyc.yale.edu/english/engl-291">Yale</a></blockquote></details>

<details><summary>poetry | Horace (19BCE) <b>Ars Poetica</b></summary><blockquote>@book{horacio65ars,
   title = {Arte poética},
   author = {Horácio and Fernandes, Raúl Miguel Rosado},
   publisher = {Livraria Clássica},
   year = {196?}}

<a href="https://en.wikipedia.org/wiki/Ars_Poetica_%28Horace%29">Wikipedia</a></blockquote></details>

#### 2021-04

<details><summary>series | <b>How To with John Wilson</b> (2020) John Wilson</summary><blockquote><a href="https://en.wikipedia.org/wiki/How_To_with_John_Wilson">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Andrei Rublev</b> (1966) Andrei Tarkovsky</summary><blockquote><a href="https://en.wikipedia.org/wiki/Andrei_Rublev_(film)">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Ivan's Childhood</b> (1962) Andrei Tarkovsky</summary><blockquote><a href="https://en.wikipedia.org/wiki/Ivan%27s_Childhood">Wikipedia</a></blockquote></details>

<details><summary>play | <b>O Dicionário da Fé</b> (Gonçalo M. Tavares) Jean Paul Bucchieri</summary><blockquote><a href="https://www.tndm.pt/pt/calendario/o-dicionario-da-fe-sala-online/">TNDM II</a></blockquote></details>

<details><summary>doc | <b>Rat Film</b> (2016) Theo Anthony</summary><blockquote><a href="https://en.wikipedia.org/wiki/Rat_Film">Wikipedia</a></blockquote></details>

#### 2021-03

<details><summary>doc | <b>Boys State</b> (2020) Jesse Moss and Amanda McBaine</summary><blockquote><a href="https://en.wikipedia.org/wiki/Boys_State_(film)">Wikipedia</a></blockquote></details>

<details><summary>doc | <b>Mão Invisível - uma história do neoliberalismo em Portugal</b> (2020) João Ramos de Almeida</summary><blockquote><a href="https://saladeimprensa.ces.uc.pt/index.php?col=canalces&id=28494#.XmZRTkPgp0s">YouTube</a></blockquote></details>

<details><summary>nonfic | Lourenço, Eduardo (1973) <b>Fernando Pessoa revisitado</b></summary><blockquote>@book{lourenco1973revisitado,
   title =     {Fernando Pessoa revisitado},
   author =    {Eduardo Louren\c{c}o},
   publisher = {Inova},
   year =      {1973}}</blockquote></details>

<details><summary>fiction | <b>Gilgamesh</b></summary><blockquote>@book{tamen2008gilgamesh,
   title =     {Gilgamesh},
   author =    {Pedro Támen and Nancy K. Sandars and Luís Alves da Costa},
   publisher = {Vega},
   isbn =      {9789726992271},
   year =      {2007},
   edition =   {4}}</blockquote></details>

#### 2021-02

<details><summary>doc | <b>Anos de Guerra - Guiné 1963-1974</b> (2000) José Barahona</summary><blockquote><a href="https://www.youtube.com/watch?v=mfSwSzRl9bM">YouTube</a></blockquote></details>

<details><summary>doc | <b>As Constituições</b> (2017) Rui Pinto de Almeida</summary><blockquote><a href="https://www.rtp.pt/programa/tv/p35061">RTP</a></blockquote></details>

<details><summary>film | <b>Blowup</b> (1966) Michelangelo Antonioni</summary><blockquote><a href="https://en.wikipedia.org/wiki/Blowup">Wikipedia</a></blockquote></details>

<details><summary>film | <b>I, Daniel Blake</b> (2016) Ken Loach</summary><blockquote><a href="https://en.wikipedia.org/wiki/I,_Daniel_Blake">Wikipedia</a></blockquote></details>

#### 2021-01

<details><summary>fiction | Voltaire (1759) <b>Candide, ou l'Optimisme</b></summary><blockquote>@book{voltaire2006candide,
   title =     {Candide and Other Stories},
   author =    {Voltaire and Roger Pearson},
   publisher = {Oxford University Press, USA},
   isbn =      {9780192807267,0192807269},
   year =      {2006},
   series =    {Oxford World's Classics}}</blockquote></details>

<details><summary>fiction | Cortázar, Julio (1968) <b>Blow-up and Other Stories</b></summary><blockquote>@book{cortazar2014blow,
   title =     {Blow-Up: And Other Stories},
   author =    {Julio Cortazar and Paul Julio Blackburn (translator)},
   publisher = {Knopf Doubleday Publishing Group},
   isbn =      {0-394-72881-5,978-0-8041-5324-9},
   year =      {2014}}</blockquote></details>
