---
layout: page
title: Procurar
---

<!-- Html Elements for Search -->
<div id="search-container">
<input type="text" id="search-input" placeholder="digita...">
<ol id="results-container"></ol>
</div>

<!-- Script pointing to search-script.js -->
<script src="/search.js" type="text/javascript"></script>

<!-- Configuration -->
<script type="text/javascript">
SimpleJekyllSearch({
  searchInput: document.getElementById('search-input'),
  resultsContainer: document.getElementById('results-container'),
  json: '/search.json',
  searchResultTemplate: '<li><a href="{url}" title="{desc}">{title}</a>  &middot {date} &middot {tags} </li>',
  noResultsText: 'Nenhum resultado encontrado.',
  limit: 10,
  fuzzy: false,
  exclude: ['Welcome']
})
</script>