---
layout: post
title:  recortes
tags: [DN, Público]
---

1.  [Diário de Notícias](#orged79b87)
2.  [Público](#orgb5d296d)


<a id="orged79b87"></a>

# Diário de Notícias

A diretora do DN parece não saber a diferença entre % e pp. A estatística diminuiu 10,4pp (pontos percentuais) e não 10,4%. É um erro tão comum que me leva a questionar se os editores do jornal não têm já uma ferramenta que os ajude a encontrá-los. Fora isto, editorial mais interessante que já li da Rosália.

«[H]ouve 60% das pessoas que estiveram num grupo com dez ou mais pessoas que revelaram não ter tido sempre a máscara posta».

Gosto de ouvir a Ana Gomes a atacar a direita neoliberal (apesar de se esquecer de alguma esquerda). Parece, no entanto, um exercício estafante apontar «neoliberalidade» quando a vemos. Por um lado, porque tudo acaba por o ser em certo nível e, por outro, porque ninguém sabe bem do que falamos.

Bom ângulo do Pedro Tadeu: «Eis um raciocínio que é um tiro de morte na Constituição: um texto constitucional que não dá escapatória para uma situação que pode provocar umas dezenas ou centenas de mortes é um texto constitucional errado».

Admito que não ia espreitar o texto do «Consultor financeiro e business developer. www.linkedin.com/in/jorgecostaoliveira» antes de ter visto a assinatura, mas valeu a pena: o *desenvolvedor* de negócios deu-me o prazer de confirmar a aversão inicial ao texto como fundamentalmente merecida.

Tenho de ler melhor a posição dos partidos sobre o fecho da central de carvão pela EDP, especialmente do BE e do PCP. A frase do sindicalista &#x2013; «[n]ão houve um período de transição justo», quanto ao facto de Portugal estar agir antes de outros países no combate às alterações climáticas &#x2013; faz lembrar a dialética do dilema dos prisioneiros.


<a id="orgb5d296d"></a>

# Público

O Manuel Carvalho diz que «havemos de pagar um preço elevadíssimo por esta opção [fechar as escolas]», mas sabemos agora ao fim do dia que essa decisão não foi tomada. Qual será o nível de exigência ou confiança que um colonista deve ter antes de falar com certeza sobre algo? Talvez eu esteja a ver isto mal. Quem sabe se o PM não decidiu como fez graças a este editorial?

A Maria João Marques escreve o artigo que eu gostava que ela escrevesse sempre.

O Andy Brown, novo líder da Galp, tem cara de novo líder da Galp.

«Ler os jornais de 1918 e 1919 é, no respeitante à pandemia, como ler os jornais de hoje — com a diferença de que então não havia Segurança Social». Que bela frase. «Pandemics Depress the Economy, Public Health Interventions Do Not». Que belo *paper*. Que bela opinião esta do Rui Tavares!

