---
layout: post
title:  recortes
tags: [DN, Público, FT]
---


1.  [Diário de Notícias](#orga1208b6)
2.  [Público](#orga0917fa)
3.  [Financial Times](#orge1b0a8c)


<a id="orga1208b6"></a>

# Diário de Notícias

Lendo pela primeira vez um artigo do Bugalho. Vou tentar levá-lo a sério sem deixar a minha envergonhada atração por meritocracia (nem uma certa inveja) se colocar no meio. Olha que até gostei&#x2026;

No outro dia disseram-me que toda a gente à esquerda era a favor do uso de psicólogos, especialmente a camada *woke*. Certamente haverá em Portugal quem tenha um discurso cauteloso quanto a certa ajuda psicológica que ajuda apenas a tolerar condições cada vez mais precárias levando por isso a uma domesticação da luta. «Pedir ajuda psicológica não é um comportamento dos fracos, mas antes um sinal de que se acredita num processo de mudança e que se está disposto a percorrer esse caminho». Saúdo a existência de pessoas dispostas a não percorrer certos caminhos. «Eu já pedi ajuda psicológica. E você?». Vão com calma e, por favor, não nos roubem o sofrimento existencial.

Gosto do João Almeida Moreira; parece ser um colonista *freelancer*. Não relacionado por algo mais que o acaso de uma procura na *Wikipedia*, o José Augusto Moreira de Almeida foi «um jornalista e político monárquico português, deputado e líder do movimento de restauração monárquica durante a Primeira República Portuguesa. Foi diretor do influente jornal lisboeta O Dia». Assim arranjei o meu facto histórico do dia.

Com o Rodolfo Gil, já vamos em dois embaixadores a darem opinião hoje. É um texto interessante. À primeira, diria que o objetivo destes exercícios é aumentar negócios através de publicidade, mas, pelo conteúdo de ambos, parece ser antes um exercício de partilha cultural.

O DN continua a pôr os seus três críticos de cinema a escrever artigos &#x2013; que bem que ficam ao lado uns dos outros. Bem-bom!


<a id="orga0917fa"></a>

# Público

«A lei estipula que os tempos máximos de resposta nas cirurgias oncológicas são de 15 dias (muito prioritárias), 45 dias (prioritárias) e 60 dias (prioridade normal). Nas cirurgias não oncológicas os prazos máximos são mais alargados — 60 dias para as intervenções cirúrgicas prioritárias e 180 dias para as de prioridade normal».

O &#x2013; literal &#x2013; entre parênteses «errámos ao acreditar que fechariam mesmo» do Manuel Carvalho vai de encontro à minha dúvida de ontem. Apesar de admitir o erro factual, não admite o erro de escrever o que escreveu. Talvez bem.

Parece que vou voltar ao acordo antigo nestes meus funestos recortes. (Lembrete para voltar para alterar a grafia e as duplas aspas).

«Como se justificam os erros nesse documento? Creio que apenas perante a urgência e pressão para apresentar um documento rapidamente, traduzi-lo para inglês e remetê-lo. Não foram erros premeditados ou conscientes da DGPJ». Deviam usar o [LanguageTool](https://languagetool.org/).

«Contactado o Ministério das Infra-Estruturas e da Habitação, fonte oficial respondeu que “o Governo cumpre todas as regras europeias relevantes sobre o Espaço Ferroviário Europeu sem, no entanto, abdicar dos instrumentos de uma política pública para o transporte ferroviário de passageiros, tal como para os outros modos de transporte coletivo”». Amo.


<a id="orge1b0a8c"></a>

# Financial Times

«Mr Kazmi said he thought the ECB was effectively targeting sovereign bond spreads in the periphery, meaning it would use its purchases to limit any rise in yields. That was encouraging investors to buy any debt that offers higher interest rates than Germany’s deeply negative-yielding bonds». Certamente, parte da explicação para os resultados alcançados pelo IGCP, noticiados assim no Jornal de Negócios: «Numa emissão de obrigações de 500 milhões de euros, o Tesouro português conseguiu uma taxa de -0,012%. Ou seja, Portugal passou a fazer parte de um grupo de países aos quais os investidores estão dispostos a pagar para emprestar dinheiro. É claro que o respaldo do BCE é fundamental, mas isso não tira mérito ao IGCP».

A leitura longa do FT é dedicada às eleições no CDU alemão. Quanto à questão da indefenição ideológica, é dito que «[t]he contest has crystallised awkward questions about what kind of party the CDU wants to be. Ms Merkel steered it towards the centre ground of German politics, often in the teeth of dogged resistance from conservatives. She abolished compulsory military service, ordered the closure of Germany’s nuclear power stations, introduced gay marriage and a national minimum wage and vastly increased childcare for working families. And then, at the height of the European migration crisis, she welcomed more than a million refugees into Germany». «It was like a big jellyfish that sucked the air out of the other parties». Também é feito uma análise do candidato à frente nas sondagens, Merz, que é um milionário e /chairman/ da /BlackRock/. «Such language has made him a hero to the CDU rank-and-file. But large parts of the CDU establishment see him as irascible, uncontrollable and too thin-skinned. And there is widespread scepticism about his plan to tilt the party rightward. Some experts think he would lose far more centrist voters to the Greens and SPD than he would gain from the AfD. That could leadto a Merz-led CDU losing the next election to a coalition of the Greens, SPD and the far-left Die Linke party».

«Relations are cordial for now. “China rocks,” according to Mr Musk, who has been praised in state media».

