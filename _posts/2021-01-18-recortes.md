---
layout: post
title:  recortes
tags: [DN, Público, FT, WaPo]
---

1.  [Diário de Notícias](#org0493323)
2.  [Público](#org4d8b4f6)
3.  [Financial Times](#orga5e8669)
4.  [Washington Post](#org891cad8)


<a id="org0493323"></a>

# Diário de Notícias

A Assunção Cristas trouxe-nos um artigo chatíssimo sobre o teletrabalho. Desconhecia ser coordenadora do Mestrado em direito e Economia do Mar na *Nova School of Law*.

O Henrique Burnay escreve sobre as «condições» europeias ao governo. «O governo, este ou outro, e as oposições, estas ou outras, têm de assumir que as imposições da União Europeia à política nacional é matéria política que extravasa, e muito, a mera competência governamental. Não sabermos, exatamente, onde vão ser gastos “os milhões europeus”, ou sabemos apenas vagamente, é uma desresponsabilização coletiva. Controlar a transparência do seu uso, como o parlamento decidiu, é um bom princípio, mas só resolve parte do problema».


<a id="org4d8b4f6"></a>

# Público

«E nós? Será que o ímpeto inato da igualdade morreu em nós? Não bastam esmolas nem remendos. A cada um a responsabilidade de lutar pela igualdade. Porque há desigualdade, há pobreza, há crise e há um mundo natural a ser destruído em nome do lucro». Isabel do Carmo, professora na FMUL, agracia-nos com um manifesto contra a desigualdade.

O Ricardo Cabral, do ISEG, propõe que presidência portuguesa da UE proteja as economias pequenas e periféricas das egoístas: «Entre outros aspectos, são apontados nesses artigos que os restantes Estados-membros foram excluídos das negociações e, segundo a WirtschaftsWoche, a própria Comissão Europeia não estaria a par de condições especiais (i.e., acordos paralelos) oferecidos à França e à Alemanha. Em particular, as autoridades chinesas estariam disponíveis para atribuir à Deutsche Telekom uma licença para operar no mercado chinês». «É tudo muito previsível! A Alemanha e a França beneficiam da União Europeia não somente porque lhes assegura um mercado com maiores economias de escala para os seus produtos, mas também porque lhes dá um maior poder negocial face a blocos económicos do resto do mundo, poder que não teriam como nações estado. Levam a União Europeia a celebrar acordos e a tomar decisões em função dos seus interesses nacionais, como o acordo com a China, a concretizar-se na actual forma, o parece demonstrar».


<a id="orga5e8669"></a>

# Financial Times

«Armin Laschet could scarcely conceal his delight last year when the carnival club in his hometown of Aachen named him an honorary knight. “Finally I get a job on the first attempt, without having to lose twice first,” he said. Faced with the kind of painful setbacks Mr Laschet has suffered in his career, most politicians would have given up and tried something else. He slipped off the greasy pole so many times that some thought he would never get back on it again». «Though everyone knew he was a Merkel-ite moderate, the cabinet he put together reflected the full spectrum of views in the CDU. He has a tough law-and-order interior minister who has launched high-profile raids on criminal clans. He has a labour minister who is a well-known expert on social policy from the left of the CDU. And he has Ms Güler, the integration secretary, who is the daughter of Turkish immigrants».

«The European Central Bank is threatening to impose additional capital requirements on banks that continue to ignore requests to rein in risk in the booming leveraged loanmarket». «Deutsche Bank received a request [by the ECB] last to suspend part of its leveraged finance business because of shortcomings in its risk controls, but it refused, the Financial Times has reported. Deutsche Bank declined to comment».

«ESG [Environmental, Social, and Governance] funds in Europe attracted net inflows of €151bn between January and October last year, up almost 78 per cent from the same period in 2019, according to Morningstar. Yet the boom has been overshadowed by concerns that some providers have been overstating their sustainability credentials to win business, a trend known as greenwashing.»

O *Big Read* de hoje é sobre produção de carne. «“[Farmers] have lost control of the narrative to those who are extremely loud,” says Mrs Angus who, with her husband and four children, raises 35,000 head of cattle on more than 160,000 hectares of land — an area slightly larger than Greater London — in Queensland». «In China, for example, some technology-focused pig growers are using facial recognition to monitor each pig and its wellbeing, while Brazil’s JBS has pledged to use blockchain technology to ensure traceability of its cattle and meat after facing accusations of “cattle laundering” — where animals from illegally deforested land are brought to legitimate cattle ranches that supply meat companies». «With the global population forecast to increase by a quarter to almost 10bn by 2050, pushing up demand for protein, the world is going to need a variety of sources, ranging from animals to cul-ured meatmade inbioreactors to plant-based substitutes».

«Union membership in the UK has been ticking up, though it is still half the 1979 peak. The most recent available data for the whole workforce, shows 6.4m Britons, or 23.5 per cent of the workforce, were card-carrying members. In the US, union membership continues to decline, to 11.2 per cent or 13.2m workers, in 2019. But the maturation of Big Tech businesses may help organisers».


<a id="org891cad8"></a>

# Washington Post

«Secretary of State Mike Pompeo has made near-daily announcements of major foreign policy actions, many of which appear designed to cement Trump priorities and create roadblocks to new directions charted by the incoming Biden team. Among the barriers put in place are the relisting of Cuba as a state sponsor of terrorism, the designation of Yemen’s Houthi rebels as terrorists, the removal of longstanding restrictions on contacts between senior U.S. officials and their Taiwanese counterparts, the recognition of Moroccan sovereignty over the long-contested Western Sahara, the fast-track approval of controversial arms sales, and a slew of new sanctions against Iran».

«For a nationwide network of left-wing activists who seek out and publish the identities of those they believe to be violent “fascists,” some investigations can take months, years even. Or it can take 10 minutes. That’s how much time Molly Conger spent on her laptop last month searching for the man who used the right-wing social media site Parler to share that he was a police officer and pledge support to a member of the Proud Boys extremist group, advocating violence against Supreme Court Chief Justice John G. Roberts Jr».

«Blackouts are not new in Iran, where an aging and subsidized electricity sector is plagued by alleged mismanagement. But this time, government officials are laying at least some of the blame on bitcoin mining at cryptocurrency farms — the energy-intensive business of using large collections of computers to verify digital coin transactions».

