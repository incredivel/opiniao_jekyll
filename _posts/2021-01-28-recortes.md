---
layout: post
title:  recortes
tags: [DN, Público, Sábado, FT]
---

1.  [Diário de Notícias](#org28b28ee)
2.  [Público](#org3d56e06)
3.  [Sábado](#org164874a)
4.  [Financial Times](#orgd7a85a2)


<a id="org28b28ee"></a>

# Diário de Notícias

«[O] sistema de oxigénio de que depende mais de uma centena de doentes colapsou». Não percebo a utilidade de usar esta linguagem hiperbólica no editorial. Mentir e exagerar passam como indendência no meu manual. «Mas adiar a vacina aos profissionais de saúde de unidades privadas – que atendem doentes diariamente com risco para todos – é criminoso». Isto não foi já desmentido? Todos os profissionais de saúde envolvidos directamente com o tratamento de doentes covid estão a ser vacinados, [não](https://twitter.com/Shyznogud/status/1354581667298553858)?

«PSP não diz o que vai fazer aos condenados no processo Cova da Moura, cuja pena por agressão, sequestro e falsificação de documento transitou em julgado. Estão os oito ao serviço, embora três tenham uma pena de mais de três anos de prisão, “infração disciplinar muito grave” que implica “demissão” ou “aposentação compulsiva”».

«Pressionado por todos os lados, o governo, mantendo as escolas fechadas, acelera o ensino online». Todos? 

«O défice público do primeiro ano da pandemia, 2020, superou os 10,3 mil milhões de euros, mas ainda assim este enorme desequilíbrio fica abaixo dos dois défices do último governo PS, de José Sócrates. Em 2009, o primeiro ano de crise económica que se seguiu à crise financeiro, o desvio atingiu um recorde de 13,4 mil milhões de euros. Em 2010, quando já se estava a formar a crise nos mercados da dívida soberana, o défice atingiu o segundo maior valor (11,5 mil milhões de euros) num levantamento feito pelo Dinheiro Vivo que recua a 2003». Tem a sua piada que continuem a insinuar que a culpa dos défices foi toda do Sócrates.

O João Moreira actualiza-nos com notícias brasileiras. «Enquanto na União Europeia, no Reino Unido e até nos Estados Unidos nem se questiona a distribuição gratuita da vacina pelo Estado, clínicas privadas do Brasil tentam comprar doses na Índia para distribuir por funcionários, parentes, clientes, enfim, pela sua casta». «Em Jupi, Pernambuco, os 300 médicos na linha da frente da cidade precisavam de 600 doses. Chegaram 68. Sobram 66, porque os dois primeiros vacinados foram a secretária da Saúde local, num momento registado com pompa em fotografia, e o autor da dita foto, o fotógrafo oficial da prefeitura, que aproveitou “o embalo”, segundo o próprio, mesmo não sendo nem médico, nem enfermeiro, nem doente, nem idoso, só um oportunista egoísta».

«A votação final da despenalização da morte clinicamente assistida na Assembleia da República está marcada para esta sexta-feira, depois de aprovados os projetos de PS, BE, PAN, PEV e Iniciativa Liberal na generalidade, há cerca de uma semana». Enquanto a garantia da dignidade humana continuar a depender de «solidariedade», o PCP faz bem em ser conservador.


<a id="org3d56e06"></a>

# Público

Na capa: «80% das máscaras certificadas pela têxtil não cumprem novas normas». Na notícia: «Se o Centro Europeu para Prevenção e Controlo das Doenças (ECDC, na sigla inglesa) apertar os critérios para as máscaras sociais, muitas das que foram certificadas pela indústria têxtil no último ano terão de ser melhoradas, ou então terão os dias contados».

O Público agora é pago para referir o jornal italiano *La Repubblica* em todos os artigos? Já contei umas 3 referências só nesta edição.

«Reforço do investimento em infra-estruturas, criando uma rede pública de lares, e aumento do número de trabalhadores permanentes são as medidas “urgentes e imprescindíveis” que o PCP vai reclamar junto do Governo no debate de actualidade sobre “os problemas nos lares de idosos” que marcou para hoje no Parlamento».

«O Conselho da União Europeia deu por encerrada a polémica sobre a nomeação do procurador europeu José Guerra, considerando não existirem razões para alterar a sua escolha para o cargo em detrimento da candidata portuguesa preferida pelo júri internacional, Ana Carla Almeida, que tinha pedido esclarecimentos sobre os fundamentos dessa decisão».

Carlos Guimarães Pinto a admitir ser o criador de propaganda questionável. «Li com atenção o artigo da jornalista Bárbara Reis, que mencionou um “autocolante” que recebeu por WhatsApp com uma comparação entre Portugal e a Irlanda. Pela descrição, esse autocolante refere-se a um quadro comparativo publicado pela Iniciativa Liberal a 13 de Dezembro de 2018 que depois circulou sem a respectiva identificação. Tendo sido eu a elaborá-lo e a escolher os dados que lá foram incluídos, é da minha inteira responsabilidade qualquer erro ou alegada desinformação apontada por Bárbara Reis».

O JMT enterrou o CDS-PP. «O CDS já foi tudo e um par de botas ao longo da sua história democrática, de elitista a populista, de centrista a extremista, de antieuropeísta a europeísta, de saudosista a reformista, o que lhe permitiu albergar no seu seio desde os expropriados do PREC aos liberais chiques como Mesquita Nunes, que ainda há oito meses era apontado por Carlos Guimarães Pinto, ex-líder da Iniciativa Liberal, como o candidato perfeito da IL à Presidência da República. Só que esse tempo de albergue ideológico acabou, pela simples razão de que o albergue deu lugar a um Airbnb de apartamentos individuais». «“Daqui a um ano, será tarde de mais”, escreve Mesquita Nunes. Infelizmente, não é daqui a um ano. É agora. Agora já é tarde de mais. O seu artigo só faz sentido se aquilo que ele estiver a tentar definir não for o futuro do CDS, mas sim o seu próprio futuro, preparando uma saída honrosa do partido (“tentei, esforcei-me, lamento, não deu”), para depois colocar o seu indiscutível talento político ao serviço de projectos que possam ter alguma ambição de crescimento a curto ou médio prazo, seja no PSD, seja na Iniciativa Liberal. O CDS é hoje um zombie no sistema político português. Está morto ou condenado à irrelevância, e a certidão de óbito chegará nas próximas legislativas. O artigo de opinião de Adolfo Mesquita Nunes não é um “aqui estou” — é um “adeus”».


<a id="org164874a"></a>

# Sábado

«O ficheiro mais secreto do Banco de Portugal vai continuar num cofre. Há anos que governantes, deputados e arguidos do caso BES andam atrás de um famoso relatório sobre a atuação do banco central na resolução do Banco Espírito Santo. Centeno entreabriu a porta, mas o Tribunal da Relação de Lisboa decidiu que o documento deve continuar em sigilo».

Esta não estava à espera. Escreveram um pequeno texto sobre *incels*. «o conseguem Os mal-amados  ter uma vida sexual, mas acreditam que têm direito a ela. Nos casos extremos, vingam-se, matando as mulheres que os ignoram. São os “incels”: celibatários involuntários.» 


<a id="orgd7a85a2"></a>

# Financial Times

«Warsaw's move to tighten abortion laws comes amid a broader push by Law and Justice, which came to power in 2015, to promote tradicional, Catholic values».

«The joke goes like this: when two Donald Trump supporters die and go to heaven, God meets them at the pearly gates. “Tell us,” they say, “what were the real results of the 2020 election and who was behind the fraud?” God answers: “My children, there was no fraud.” After a few seconds of stunned silence, one turns to the other, whispering: “This goes higher up than we thought”».

«Talk of hydrogen as the fuel of the future goes back to the 19th century. Its promise is starting to be realised thanks to falling costs and its scope for curbing greenhouse gas emissions. A French hydrogen refuelling company's initial public offering is the latest sign of enthusiasm for green energy companies».

