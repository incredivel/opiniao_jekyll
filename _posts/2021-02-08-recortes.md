---
layout: post
title:  recortes
tags: [DN, Público, Avante, FT]
---

1.  [Diário de Notícias](#orgff70d9b)
2.  [Público](#orgf4f7ed0)
3.  [Avante](#org2958adb)
4.  [Financial Times](#orgbb439a3)


<a id="orgff70d9b"></a>

# Diário de Notícias

[E quando menos se espera&#x2026; o contra-ataque](https://www.dn.pt/edicao-do-dia/08-fev-2021/e-quando-menos-se-espera-o-contra-ataque-13326279.html), Editorial. Se vejo mais alguma referência à necessidade de a mulher de César ter de parecer honesta, sofro um enfarte do miocárdio. Estes editoriais do DN estão cada vez mais alucinados. «Não será necessário abrir uma guerra, mas Biden já percebeu que - mesmo mantendo o seu tom de voz calmo e educado -, não pode, nem por um segundo, ser anjinho na sua relação com o Império do Meio».

[Democracia sim, mas não para os inimigos da democracia?](https://www.dn.pt/edicao-do-dia/08-fev-2021/democracia-sim-mas-nao-para-os-inimigos-da-democracia-13326529.html). «O argumento surge algo paradoxal: para merecer a ilegalização, o partido teria então de ser muito mais poderoso. É preciso deixar sair a serpente do ovo, portanto - que haja danos a sério».

[Mais de 50 partidos foram ilegalizados nas democracias europeias desde 1945](https://www.dn.pt/edicao-do-dia/08-fev-2021/mais-de-50-partidos-foram-ilegalizados-nas-democracias-europeias-desde-1945-13326758.html). «Em Portugal houve outro processo movido pelo MP para ilegalização de uma organização política, o MAN (Movimento de Ação Nacional), de extrema-direita, fundado em 1985. Mas o Tribunal Constitucional decidiu, em 1994, não decidir, uma vez que a organização fora entretanto extinta».

[Receita de IRS bate recorde no ano da maior recessão da democracia](https://www.dn.pt/dinheiro/receita-de-irs-bate-recorde-no-ano-da-maior-recessao-da-democracia-13326699.html). «"Esta não é uma crise da burguesia. Os trabalhadores dos setores intelectuais, que não implicam deslocações, ficaram em casa em teletrabalho e não foram afetados", frisa. "O caso do aumento da receita do IRS é uma demonstração clara de que esta crise representa um agravamento das desigualdades. Tem uma dimensão de classe", conclui».


<a id="orgf4f7ed0"></a>

# Público

[Agora com Internet e computador, Samuel já pode fazer mais de três fichas por dia](https://www.publico.pt/2021/02/08/local/noticia/internet-computador-samuel-ja-tres-fichas-dia-1949539). «Além do risco de estragar o computador emprestado pela escola, Ana Paula preocupa-se por não saber trabalhar com a máquina. “Sei que vai exigir muito porque aquilo é chinês para mim. Ele [o Samuel], se calhar, desenrasca-se melhor do que eu.” Ao mesmo tempo pensa nos miúdos que vivem com os avós, pessoas mais velhas que nunca tocaram num aparelho daqueles: “Como é que vão ensinar os netos? É um bocado complicado”».

[A preocupação com a dívida pública](https://www.publico.pt/2021/02/08/economia/opiniao/preocupacao-divida-publica-1949710), Ricardo Cabral. «Ou seja, a mensagem mais importante é que a dívida pública, apesar de muito elevada, não deve preocupar os nossos decisores. A prioridade deve ser colocar a economia a crescer, o que automaticamente colocaria a dívida pública em percentagem do PIB numa trajectória descendente, i.e., numa trajectória sustentável».

[Na Polónia, a Igreja uniu-se à política e os católicos afastam-se](https://www.publico.pt/2021/02/08/mundo/noticia/polonia-igreja-juntase-politica-catolicos-afastamse-1949133). «Para a mais jovem geração de polacos, a aliança entre o nacionalismo e a piedade - entre o PiS e a Igreja Católica - é nefasta e está a gerar uma sociedade de amarras e proibições. O aborto foi proibido, a comunidade LGBT é perseguida, os padres fazem comícios e há o lastro dos abusos sexuais».


<a id="org2958adb"></a>

# Avante

[Aprovada lei da eutanásia](https://avante.pt/pt/2462/assembleiadarepublica/162436/Aprovada-lei-da-eutan%C3%A1sia.htm). «O que os deputados foram chamados a aprovar não foi porém o «julgamento da consciência individual de cada um». O que está e causa são as «consequências sociais profundamente negativas que podem advir da opção» agora tomada pelo legislador, fez notar o parlamentar comunista».


<a id="orgbb439a3"></a>

# Financial Times

[Now is the time to reform the UK’s dysfunctional tax system](https://www.ft.com/content/06830ba4-2a50-4db1-93d0-3f03ce3d5350), Martin Wolf.

